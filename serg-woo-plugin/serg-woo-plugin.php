<?php
/*
Plugin Name: Serg Woo Plugin
Plugin URI: http://localhost
Description:
Version: 1.0
Author: Sergey
Author URI:
*/

add_action( 'woocommerce_after_order_notes', 'create_checkout_field' );

function create_checkout_field( $checkout ) {

	echo '<div id="additional_checkout_field">';

	woocommerce_form_field( 'additional_field_name', array(
		'type'        => 'text',
		'class'       => array( 'additional-checkout-class form-row-wide' ),
		'label'       => __( 'Additional information' ),
		'placeholder' => __( 'Enter something' ),
	), $checkout->get_value( 'additional_field_name' ) );

	echo '</div>';
}

add_action( 'woocommerce_checkout_process', 'create_checkout_field_process' );

function create_checkout_field_process() {
	if ( ! $_POST['additional_field_name'] ) {
		wc_add_notice( __( 'Please enter something into this new shiny field.' ), 'error' );
	}
}

add_action( 'woocommerce_checkout_update_order_meta', 'additional_checkout_field_update_order_meta' );

function additional_checkout_field_update_order_meta( $order_id ) {
	if ( ! empty( $_POST['additional_field_name'] ) ) {
		update_post_meta( $order_id, 'Additional information', sanitize_text_field( $_POST['additional_field_name'] ) );
	}
}

add_action( 'woocommerce_admin_order_data_after_billing_address', 'additional_checkout_field_display_admin_order_meta', 10, 1 );

function additional_checkout_field_display_admin_order_meta( $order ) {
	echo '<p><strong>' . __( 'Additional information' ) . ':</strong> ' . get_post_meta( $order->id, 'Additional information', true ) . '</p>';
}

add_filter( 'woocommerce_email_order_meta_keys', 'additional_order_meta_keys' );

function additional_order_meta_keys( $keys ) {
	$keys[] = 'Additional information';

	return $keys;
}
